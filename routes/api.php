<?php

use App\Http\Controllers\Buyer\BuyerCategoryController;
use App\Http\Controllers\Buyer\BuyerProductController;
use App\Http\Controllers\Buyer\BuyersController;
use App\Http\Controllers\Buyer\BuyerSellerController;
use App\Http\Controllers\Buyer\BuyerTransactionController;
use App\Http\Controllers\Category\CategoriesController;
use App\Http\Controllers\Category\CategoryBuyerController;
use App\Http\Controllers\Category\CategoryProductController;
use App\Http\Controllers\Category\CategorySellerController;
use App\Http\Controllers\Category\CategoryTransactionController;
use App\Http\Controllers\Product\ProductBuyerController;
use App\Http\Controllers\Product\ProductBuyerTransactionController;
use App\Http\Controllers\Product\ProductCategoryController;
use App\Http\Controllers\Product\ProductsController;
use App\Http\Controllers\Product\ProductTransactionController;
use App\Http\Controllers\Seller\SellerBuyerController;
use App\Http\Controllers\Seller\SellerCategoryController;
use App\Http\Controllers\Seller\SellerProductController;
use App\Http\Controllers\Seller\SellersController;
use App\Http\Controllers\Seller\SellerTransactionController;
use App\Http\Controllers\Transaction\TransactionCategoryController;
use App\Http\Controllers\Transaction\TransactionsController;
use App\Http\Controllers\Transaction\TransactionSellerController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Support\Facades\Route;

/*
 * Buyers Routes
 */
//Route::resource('buyers', BuyersController::class, ['only'=>['index', 'show']]);
Route::resource('buyers', BuyersController::class)->only('index', 'show');
Route::resource('buyers.transactions', BuyerTransactionController::class)->only('index');
Route::resource('buyers.sellers', BuyerSellerController::class)->only('index');
Route::resource('buyers.products', BuyerProductController::class)->only('index');
Route::resource('buyers.categories', BuyerCategoryController::class)->only('index');

/*
 * Categories Routes
 */
Route::resource('categories', CategoriesController::class)->except('create', 'edit');
Route::resource('categories.products', CategoryProductController::class)->only('index');
Route::resource('categories.transactions', CategoryTransactionController::class)->only('index');
Route::resource('categories.buyers', CategoryBuyerController::class)->only('index');
Route::resource('categories.sellers', CategorySellerController::class)->only('index');

/*
 * Products Routes
 */
Route::resource('products', ProductsController::class)->only('index', 'show');
Route::resource('products.transactions', ProductTransactionController::class)
    ->only('index');
Route::resource('products.buyers', ProductBuyerController::class)->only('index');
Route::resource('products.categories', ProductCategoryController::class)
    ->only('index', 'update', 'destroy');
Route::resource('products.buyers.transactions', ProductBuyerTransactionController::class)
    ->only('store');

/*
 * Sellers Routes
 */
Route::resource('sellers', SellersController::class)->only('index', 'show');
Route::resource('sellers.transactions', SellerTransactionController::class)
    ->only('index');
Route::resource('sellers.categories', SellerCategoryController::class)
    ->only('index');
Route::resource('sellers.buyers', SellerBuyerController::class)
    ->only('index');
Route::resource('sellers.products', SellerProductController::class)
    ->except('edit', 'create', 'show');

/*
 * Transactions Routes
 */
Route::resource('transactions', TransactionsController::class)->only('index', 'show');
Route::resource('transactions.categories', TransactionCategoryController::class)->only('index');
Route::resource('transactions.sellers', TransactionSellerController::class)->only('index');

/*
 * Users Routes
 */
Route::resource('users', \App\Http\Controllers\User\UsersController::class)->except('create', 'edit');
Route::get('users/verify/{token}', [\App\Http\Controllers\User\UsersController::class, 'verify'])->name('users.verify');
Route::get('users/{user}/resend-verification-email', [\App\Http\Controllers\User\UsersController::class, 'resend'])->name('users.resend');

Laravel\Passport\Passport::routes();
