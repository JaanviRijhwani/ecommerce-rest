<?php

namespace App\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'identifier' => (int) $product->id,
            'title' => $product->name,
            'description' => $product->description,
            'quantity' => (int)$product->quantity,
            'status' => (string)$product->status,
            'picture' => url("img/{$product->image}"),
            'seller' => $product->seller_id,
            'creationDate' => $product->created_at,
            'lastChangeDate' => $product->updated_at,
            'deletionDate' => $product->deleted_at ?? null,

            /* HATEOS Implementation */

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('products.show', $product->id)
                ],
                [
                    'rel' => 'products.categories',
                    'href' => route('products.categories.index', $product->id)
                ],
                [
                    'rel' => 'products.transactions',
                    'href' => route('products.transactions.index', $product->id)
                ],
                [
                    'rel' => 'products.buyers',
                    'href' => route('products.buyers.index', $product->id)
                ],
                [
                    'rel' => 'sellers',
                    'href' => route('sellers.index', $product->seller_id)
                ],
            ],

        ];
    }

    public static function getOriginalAttribute(string $transformedAttribute)
    {
        $attribute = [
            'identifier' => 'id',
            'title' => 'title',
            'description' => 'description',
            'quantity' => 'quantity',
            'status' => 'status',
            'picture' => 'image',
            'seller' => 'seller_id',
            'creationDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deleted_at',
        ];

        return $attribute[$transformedAttribute] ?? null;
    }

    public static function getTransformedAttribute(string $originalAttribute)
    {
        $attribute = [
            'id' => 'identifier',
            'title' => 'title',
            'description' => 'description',
            'quantity' => 'quantity',
            'status' => 'status',
            'image' => 'picture',
            'seller_id' => 'seller',
            'created_at' => 'creationDate',
            'updated_at' => 'lastChangeDate',
            'deleted_at' => 'deletionDate',
        ];

        return $attribute[$originalAttribute] ?? null;
    }
}
