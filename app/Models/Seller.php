<?php

namespace App\Models;

use App\Http\Controllers\Seller\SellerTransactionController;
use App\Scopes\SellerScope;
use App\Transformers\SellerTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seller extends User
{
    use HasFactory, softDeletes;

    protected $table = 'users';

    public string $transformer = SellerTransformer::class;

    public function __construct(array $attributes = [])
    {
        array_push($this->hidden, 'admin');
    }

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SellerScope);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
