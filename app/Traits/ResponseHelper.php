<?php

namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ResponseHelper
{
    private function successResponse(mixed $data, int $code)
    {
        $this->cacheResponse($data);
        return response()->json($data, $code);
    }

    protected function reportMultipleErrors(mixed $message, int $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function errorResponse(string $message, int $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, int $code = 200)
    {
        if($collection->isEmpty())
        {
            return $this->successResponse(['count' => 0, 'data' => $collection], $code);
        }

        $transformer = $collection->first()->transformer;

        $collection = $this->sort($collection, $transformer);
        $collection = $this->filter($collection, $transformer);
        //paginated collection
        $collection = $this->paginate($collection);

        $transformedCollection = $this->tranformData($collection, $transformer);
        $transformedCollection['count'] = $collection->count();
        return $this->successResponse($transformedCollection, $code);
    }

    protected function showOne(Model $model, int $code = 200)
    {
        $transformer = $model->transformer;
        $transformedData = $this->tranformData($model, $transformer);
        return $this->successResponse(['data' => $transformedData], $code);
    }

    protected function showMessage(String $message, int $code = 200)
    {
        return $this->successResponse(['data' => $message], $code);
    }

    protected function tranformData($data, string $transformer)
    {
        $tranformedData = fractal($data, new $transformer);
        return $tranformedData->toArray();
    }

    private function sort(Collection $collection, string $transformer)
    {
        if(request()->has('sort_by'))
        {
            $transformedAttribute = request()->sort_by;
            $sortByAttribute = $transformer::getOriginalAttribute($transformedAttribute);
            $collection = $collection->sortBy($sortByAttribute);
        }

        return $collection;
    }

    private function filter(Collection $collection, string $transformer)
    {
        foreach (request()->query() as $filterBy => $value) {
            if($this->isFilterableAttribute($filterBy)){
                $actualAttribute = $transformer::getOriginalAttribute($filterBy);
                if(isset($actualAttribute, $value)) {
                    $collection = $collection->where($actualAttribute, $value);
                }
            }
        }

        return $collection;
    }


    private function isFilterableAttribute(string $attribute):bool {
        return ! in_array($attribute, ['sort_by']);
    }

    private function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:10|max:100'
        ];

        Validator::validate(request()->all(), $rules);


        $page = LengthAwarePaginator::resolveCurrentPage();
        $elementsPerPage = 15;

        if(request()->has('per_page')) {
            $elementsPerPage = (int)request()->per_page;
        }
        //konse page pe kitna hai, ye sab sikhaya hai core php mei
        //$elementsPerPage is current page
        $result = $collection->slice($elementsPerPage * ($page-1), $elementsPerPage);
        $paginator = new LengthAwarePaginator($result, $collection->count(), $elementsPerPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPage()
        ]);
        //as you change the page req then the other req like sortby is not preserved!
        //so to preserve all the request
        $paginator->appends(request()->all());
        return $paginator;
    }

    private function cacheResponse(mixed $data)
    {
        $url = request()->url();
        $queryParameters = request()->query(); //gives us key value pair

        $method = request()->getMethod();

        ksort($queryParameters); //sorts according to key

        $queryString = http_build_query($queryParameters); //it gives us query string

        $fullUrl = "$method:{$url}?{$queryString}";

        return Cache::remember($fullUrl, 30, function () use($data){
            return $data;
        });
    }
}
