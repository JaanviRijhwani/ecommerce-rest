<?php


namespace App\Http\Controllers\Product;


use App\Http\Controllers\Category\CategorySellerController;
use App\Models\Category;
use App\Models\Product;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;


class ProductCategoryController extends \App\Http\Controllers\ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only('index');
        $this->middleware('auth:api')->except('index');
        $this->middleware('scope:manage-product')->except('store');

        $this->middleware('can:add-category,product')->except('update');
        $this->middleware('can:delete-category,product')->except('destroy');
    }

    public function index(Product $product)
    {
        $categories = $product->categories;
        return $this->showAll($categories);
    }

    public function update(Request $request, Product $product, Category $category)
    {
        $product->categories()->syncWithoutDetaching($category->id);
        $categories = $product->categories;
        return $this->showAll($categories);
    }

    public function destroy(Product $product, Category $category)
    {
        if(! $product->categories()->find($category->id)) {
            return $this->errorResponse('The product doesnot belong to the specified category', 404);
        }
        $product->categories()->detach($category->id);
        $categories = $product->categories;
        return $this->showAll($categories);
    }
}
