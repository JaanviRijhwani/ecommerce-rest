<?php

namespace App\Http\Controllers;

use App\Models\Transaction;

class TransactionSellerController extends ApiController
{
    public function index(Transaction $transaction)
    {
        $seller = $transaction->product->seller;
        return $this->showOne($seller);
    }
}
