<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApplicationSignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, String $header = 'X-Name')
    {
        $response = $next($request);
        $response->headers->set($header, config('app.name'));
        // process: i will change the response
        //After Middleware
        return $response;
    }
}
